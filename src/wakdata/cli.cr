require "option_parser"
require "../../lib/wakdata/src/wakdata"
require "../../lib/wakdata-json/src/wakdata/json"

OptionParser.parse do |parser|
  parser.banner = "Wakdata CLI (JSON Module version : #{Wakdata::Json::VERSION}, Data Module Version : #{Wakdata::VERSION})"

  parser.on "-v", "--version", "Show latest data version" do
    begin
      puts Wakdata::Json.get_version
      exit
    rescue exception
      puts "Following exception are occured : #{exception.message}"
      exit -1
    end
  end

  parser.on "-c", "--config", "Show config" do
    begin
      puts Wakdata::Json.get_config.to_json
      exit
    rescue exception
      puts "Following exception are occured : #{exception.message}"
      exit -1
    end
  end

  parser.on "-d DIRECTORY", "--data DIRECTORY", "Retrieve data inside directory specified (Can use following joker in path for replace by version : #version#)" do |raw_directory|
    begin
      version = Wakdata::Json.get_version
      puts "Data retrieved with following version : #{version}"
      directory = raw_directory.gsub("#version#", version)
      if !Dir.exists?(directory)
        puts "Directory #{directory} missing. Created"
        Dir.mkdir_p(directory)
      end
      Wakdata::Json.get_jsons(version).each do |name, data|
        path = "#{directory}/#{name}.json"
        if File.exists?(path)
          puts "Following file #{path} exist. Move to #{path}.old"
          File.rename(path, "#{path}.old")
        end
        File.write(path, data.to_json)
      end
      exit
    rescue exception
      puts "Following exception are occured : #{exception.message}"
      exit -1
    end
  end

  parser.on "-s NAME", "--specific-data NAME", "Retrieve specific data inside file associated" do |name|
    begin
      version = Wakdata::Json.get_version
      puts "Data retrieved with following version : #{version}"
      data = Wakdata::Json.get_json(name, version)
      path = "#{name}.json"
      if File.exists?(path)
        puts "Following file #{path} exist. Move to #{path}.old"
        File.rename(path, "#{path}.old")
      end
      File.write(path, data.to_json)
      exit
    rescue exception
      puts "Following exception are occured : #{exception.message}"
      exit -1
    end
  end

  parser.on "-h", "--help", "Show help" do
    puts parser
    exit
  end

  parser.unknown_args do
    puts "Unknown args provided showing help"
    puts parser
    exit -2
  end
end
