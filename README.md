# Wakdata CLI - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-cli-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-cli-crystal/-/pipelines)

Wakdata CLI is CLI written is crystal for fetch public data of MMORPG Wakfu from JSON files distributed in Wakfu CDN

## Usage

Simple usage is described in [cli](./src/wakdata/cli.cr).

## Fetch Data

You can fetch latest data with [cli](./src/wakdata/cli.cr).

* With makefile inside repo

```bash
make fetch_data
```

* With CLI

```bash
./bin/wakdata-cli -d data/#version#
```
