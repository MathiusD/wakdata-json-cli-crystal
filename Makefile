default_target: fetch_data

clean:
	@rm -rf bin
	@rm -rf data
	@rm -rf lib
	@rm -f shard.lock

install:
	@shards install

build:
	@shards build

format:
	@crystal tool format --check

fetch_data: build
	@./bin/wakdata-cli -d data/#version#
